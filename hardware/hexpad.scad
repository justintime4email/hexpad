/*
HexPad (c) by Justin Wong

HexPad is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
*/

switch_distance = 21.59;
sqrt3 = sqrt(3);
CENTER = "center";
$fs=0.5;
cherry_hole_w = 0.551 * 25.4;
cherry_hole_offset = -0.1;

// 12 in acrylic sheet
//#offset(delta=-3)
//square(12*25.4);

// pcb outline
// offset(delta=-3)
// square([6*25.4, 5*25.4],center=true);

//translate([77.5,137])
union() {
    translate([0,0])
    switch_frame();

    translate([0, 4*switch_distance])
    switch_frame();

    translate([0,-4*switch_distance])
    switch_frame();

    translate([16*switch_distance/4*sqrt3, 4*switch_distance])
    switch_frame();
    
    translate([16*switch_distance/4*sqrt3,0])
    switch_frame();
    
    translate([16*switch_distance/4*sqrt3,-4*switch_distance])
    switch_frame();
}

frame_offset = -1.25;
    
module switch_frame(){
difference(){
offset(r=2)offset(delta=-2)
difference(){
offset(delta=-0.125)
square([16*switch_distance/4*sqrt3,4.375*switch_distance], center=true);
    translate([ switch_distance/4*sqrt3+0,-1*switch_distance/4]) {
    translate([-4*switch_distance/2*sqrt3,2.5*switch_distance])
        switch_frame_cut();
    translate([-2*switch_distance/2*sqrt3,2.5*switch_distance])
        switch_frame_cut();
    translate([ 0*switch_distance/2*sqrt3,2.5*switch_distance])
        switch_frame_cut();
    translate([ 2*switch_distance/2*sqrt3,2.5*switch_distance])
        switch_frame_cut();                   
    }
    translate([switch_distance/4*sqrt3,-switch_distance/4]) {
    translate([-3*switch_distance/2*sqrt3,-2*switch_distance])
        switch_frame_cut();
    translate([-1*switch_distance/2*sqrt3,-2*switch_distance])
        switch_frame_cut();
    translate([ 1*switch_distance/2*sqrt3,-2*switch_distance])
        switch_frame_cut();
    translate([ 3*switch_distance/2*sqrt3,-2*switch_distance])
        switch_frame_cut();        
    }     
}

translate([switch_distance/4*sqrt3,switch_distance/4]) {
for(x=[-2:1],y=[-2:1]){
     translate([ 2*x*switch_distance/2*sqrt3, y*switch_distance ]) {
	  keyswitch_hole();
	  //text(str(x+y*7 + 16), halign=CENTER, valign=CENTER);
     } 
}

for(x=[-2:1],y=[-2:1]){
     translate([ 2*(x+0.5)*switch_distance/2*sqrt3, (y+0.5)*switch_distance ]) {
	  keyswitch_hole();
	  //text(str(x+y*7+4 + 16), halign=CENTER, valign=CENTER);
     }
}
}
}
}

module keyswitch_hole() {
     difference() {
	  //circle(d=switch_distance);
	      offset(delta=cherry_hole_offset)
square(cherry_hole_w, center=true);    
     }
}

module switch_frame_cut() {
    offset(r=2)
    offset(delta=frame_offset-2)
  square(switch_distance,center=true);
    }
