#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "bsp/board.h"
#include "hardware/spi.h"
#include "hardware/gpio.h"
#include "pico/stdlib.h"
#include "tusb.h"

/* This MIDI example send sequence of note (on/off) repeatedly. To test on PC, you need to install
 * synth software and midi connection management software. On
 * - Linux (Ubuntu): install qsynth, qjackctl. Then connect TinyUSB output port to FLUID Synth input port
 * - Windows: install MIDI-OX
 * - MacOS: SimpleSynth
 */

//--------------------------------------------------------------------+
// MACRO CONSTANT TYPEDEF PROTYPES
//--------------------------------------------------------------------+

/* Blink pattern
 * - 250 ms  : device not mounted
 * - 1000 ms : device mounted
 * - 2500 ms : device is suspended
 */
enum  {
  BLINK_NOT_MOUNTED = 250,
  BLINK_MOUNTED = 1000,
  BLINK_SUSPENDED = 2500,
};

static uint32_t blink_interval_ms = BLINK_NOT_MOUNTED;

uint8_t ROWS[] = { 14, 13, 12, 11, 10,  9,  8, 15 };
/* uint8_t ROWS[] = { 8,9,10,11,12,13,14,15 }; */
uint8_t COLS[] = {  0,  1,  2,  3,  4,  5,  6,  7 };


bool   key_states[64] = { false };
bool p_key_states[64] = { false };
uint8_t midi_map[] = {   3,  0,  4,  1,  5,  2,  6,  3,
			 3,  0,  4,  1,  5,  2,  6,  3,
		        10,  7, 11,  8, 12,  9, 13, 10,
		        10,  7, 11,  8, 12,  9, 13, 10,
			17, 14, 18, 15, 19, 16, 20, 17,
			17, 14, 18, 15, 19, 16, 20, 17,
			24, 21, 25, 22, 26, 23, 27, 24,
			24, 21, 25, 22, 26, 23, 27, 24 };

uint8_t note_offset = 48;

uint8_t prog_map[] = {   1,  1,  2,  2,  3,  3,  4,  4,
		         5,  5,  6,  6,  7,  7,  8,  8,
			 9,  9, 10, 10, 11, 11, 12, 12,
			13, 13, 14, 14, 15, 15, 16, 16,
			17, 17, 18, 18, 19, 19, 20, 20,
			21, 21, 22, 22, 23, 23, 24, 24,
			25, 25, 26, 26, 27, 27, 28, 28,
			29, 29, 30, 30, 31, 31, 32, 32 };


uint8_t program_num = 1;

static uint32_t start_ms = 0;
static bool led_state = false;

void led_blinking_task(void);
void midi_task(void);
void melody_init(void);
void button_task(void);

/*------------- MAIN -------------*/
int main(void)
{
  board_init();

  tusb_init();

  // Initialize columns
  for(uint8_t i=0; i<8; i++){
    gpio_init(COLS[i]);
    gpio_set_dir(COLS[i], GPIO_OUT);
    gpio_disable_pulls(COLS[i]);
    gpio_put(COLS[i], 0);
  }
  // Initialize Rows
  for(uint8_t i=0; i<8; i++){
    gpio_init(ROWS[i]);
    gpio_pull_down(ROWS[i]);
    gpio_put(COLS[i], 0);
    gpio_set_dir(ROWS[i], GPIO_IN);
  }
  
  while (1)
  {
    tud_task(); // tinyusb device task
    led_blinking_task();
    button_task();
  }


  return 0;
}

//--------------------------------------------------------------------+
// Device callbacks
//--------------------------------------------------------------------+

// Invoked when device is mounted
void tud_mount_cb(void)
{
  blink_interval_ms = BLINK_MOUNTED;
}

// Invoked when device is unmounted
void tud_umount_cb(void)
{
  blink_interval_ms = BLINK_NOT_MOUNTED;
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us  to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en)
{
  (void) remote_wakeup_en;
  blink_interval_ms = BLINK_SUSPENDED;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void)
{
  blink_interval_ms = BLINK_MOUNTED;
}

uint8_t START_TONE[] = { 60, 63, 67 };

void melody_init(void )
{
  uint8_t const cable_num = 0; // MIDI jack associated with USB endpoint
  uint8_t const channel   = 0; // 0 for channel 1

  uint8_t packet[4];
  while ( tud_midi_available() ) tud_midi_packet_read(packet);

  
  for(uint8_t i=0; i<3; i++) {
    uint8_t note_on[3] = { 0x90 | channel, START_TONE[i], 127 };
    tud_midi_stream_write(cable_num, note_on, 3);
    sleep_ms(250);
  }

  for(uint8_t i=0; i<3; i++) {
    uint8_t note_off[3] = { 0x80 | channel, START_TONE[i], 0 };
    tud_midi_stream_write(cable_num, note_off, 3);
  }
}
  
void button_task(void)
{
    
  // Reset rows
  for(uint8_t i=0; i<8; i++){
    gpio_pull_down(ROWS[i]);
    gpio_set_dir(ROWS[i], GPIO_IN);
    gpio_put(COLS[i], 0);
  }
  // Initialize columns
  for(uint8_t i=0; i<8; i+=2){
    gpio_set_dir(COLS[i], GPIO_OUT);
    gpio_disable_pulls(COLS[i]);
    gpio_put(COLS[i], 0);
  }
  
  // Read Packets
  uint8_t packet[4];
  while ( tud_midi_available() ) tud_midi_packet_read(packet);
  
  // Initialize Odd Rows
  for(uint8_t i=1; i<9; i+=2){
    gpio_pull_down(ROWS[i]);
    gpio_set_dir(ROWS[i], GPIO_IN);
  }
  // Initialize Even Rows
  for(uint8_t i=0; i<8; i+=2){
    gpio_disable_pulls(ROWS[i]);
    gpio_set_dir(ROWS[i], GPIO_OUT);
    gpio_put(ROWS[i], 1);
  }
  sleep_us(2);
  // Loop over Even Columns and Odd Rows
  for(uint8_t x=0; x<8; x+=2) {
    // Select Column
    gpio_put(COLS[x], 1);
    sleep_us(2);
    // Read odd rows
    for(uint8_t y=1; y<9; y+=2) {
      bool row_state = gpio_get(ROWS[y]);
      key_states[ (x + y*8 + 56) % 64 ] = row_state;
      // Set LED key state on
      // key_states[ (x + y*8) ] = !row_state;
    }
    // Write even rows
    for(uint8_t y=0; y<8; y+=2) {
      gpio_put(ROWS[y], !key_states[ (x + y*8) ] || key_states[ (x + y*8 + 8) % 64 ]);
    }
    sleep_us(10);
    // Deselect Column
    gpio_put(COLS[x], 0);
    for(uint8_t y=0; y<8; y+=2) {
      gpio_put(ROWS[y], 1);
    }
    sleep_us(100);
  }
  sleep_us(2);
  
  // Initialize Even Rows
  for(uint8_t i=0; i<8; i+=2){
    gpio_pull_down(ROWS[i]);
    gpio_set_dir(ROWS[i], GPIO_IN);
  }
  // Initialize Odd Rows
  for(uint8_t i=1; i<9; i+=2){
    gpio_disable_pulls(ROWS[i]);
    gpio_set_dir(ROWS[i], GPIO_OUT);
    gpio_put(ROWS[i], 1);
  }
  sleep_us(2);
  // Loop over Odd Columns and Even Rows
  for(uint8_t x=1; x<9; x+=2) {
    // Select Column
    gpio_put(COLS[x], 1);
    sleep_us(2);
    // Read even rows
    for(uint8_t y=0; y<8; y+=2) {
      bool row_state = gpio_get(ROWS[y]);
      key_states[ x + y*8 ] = row_state;
      // Set LED key state on
      // key_states[ (x + y*8 + 56) % 64 ] = !row_state;
    }
    // Write odd rows
    for(uint8_t y=1; y<9; y+=2) {
      gpio_put(ROWS[y], !key_states[ (x + y*8 + 72) % 64 ] || key_states[ (x + y*8) ]);
    }
    sleep_us(10);
    // Deselect Column
    gpio_put(COLS[x], 0);
    for(uint8_t y=1; y<9; y+=2) {
      gpio_put(ROWS[y], 1);
    }
    sleep_us(100);
  }
  sleep_us(2);
  
  // Reset rows
  for(uint8_t i=0; i<8; i++){
    gpio_pull_down(ROWS[i]);
    gpio_set_dir(ROWS[i], GPIO_IN);
    gpio_put(COLS[i], 0);
  }
  
  static uint32_t start_ms = 0;
  uint8_t const cable_num = 0; 
  uint8_t const channel   = 0;
  
  uint8_t fn = 0;
  // Get function keys
  if(key_states[0])  fn = fn | 0b0001;
  if(key_states[16]) fn = fn | 0b0010;
  if(key_states[32]) fn = fn | 0b0100;
  if(key_states[48]) fn = fn | 0b1000;

  // Read states
  for(uint8_t i=0; i<64; i++) {
    if(i%16 < 8) {
      uint8_t key_state_i = i;
      bool is_fn = ( (key_state_i ==  0) ||
		     (key_state_i == 16) ||
		     (key_state_i == 32) ||
		     (key_state_i == 48) );

      // Rising Edge (Key Pressed)
      if( !p_key_states[key_state_i ] &&  key_states[key_state_i ]) {
	// Change octave down
	if( key_state_i  ==  0 && note_offset > 13 )
	  note_offset -= 12;
	
	// Change octave up
	if( key_state_i  == 32 && note_offset < 83 )
	  note_offset += 12;
	  
	if( !is_fn && fn == 0b1000 ) {
	  // Program change
	  program_num = prog_map[key_state_i ];
	  if(program_num != 0) {
	    uint8_t prog_ch[3] = { 0xC0 | channel, program_num, program_num };
	    tud_midi_stream_write(cable_num, prog_ch, 3);
	    // Play demo note middle c
	    // uint8_t note_on[3] = { 0x90 | channel, 60, 127 };
	    // tud_midi_stream_write(cable_num, note_on, 3);
	  }

	}
	if( fn==0 ) {
	  // Play note on
	  uint8_t note_on[3] = { 0x90 | channel, midi_map[key_state_i]+note_offset, 127 };
	  tud_midi_stream_write(cable_num, note_on, 3);
	}
      }
      // Falling Edge (Key Released)
      if(  p_key_states[key_state_i ] && !key_states[key_state_i ] ) {
	// Play note off
	if( fn==0 ) {
	  uint8_t note_off[3] = { 0x80 | channel, midi_map[key_state_i ]+note_offset, 0 };
	  tud_midi_stream_write(cable_num, note_off, 3);
	}
      }
    }
  }
  for(uint8_t i=0; i<64; i++){
    p_key_states[i] = key_states[i];
  }
  
}
//--------------------------------------------------------------------+
// BLINKING TASK
//--------------------------------------------------------------------+
void led_blinking_task(void)
{

  // Blink every interval ms
  if ( board_millis() - start_ms < blink_interval_ms) return; // not enough time
  start_ms += blink_interval_ms;

  board_led_write(led_state);
  led_state = 1 - led_state; // toggle
}
